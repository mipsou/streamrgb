# StreamRGB

StreamRGB est l'intégration parfaite entre vos appareils et les lumières RVB.
Créateur de contenu sur Twitch, YouTube, Facebook ou d'autres plateformes de streaming, StreamRGB apporte du contenu visuel interactif avec les spectateurs.

Avec votre Bot, signalez à vos spectateurs de taper !Vert , interactifs comme !Hex #ff00ff , ou l'une des innombrables options personnalisées que vous pouvez créer et choisir et regarder les changements de lumière.